request_id: 1
 session_config {
  buffers {
   pages: 16384
  }
 }
 plugin_configs {
  plugin_name: "hisysevent-plugin"
  config_data {
   msg: "profiler test go go go",
   subscribe_domain: "PROFILER"
  }
 }